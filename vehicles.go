package main

import (
	"encoding/json"
	"strings"
)

type Vehicle struct {
	Name      string  `json:"name"`
	Hex       float64 `json:"hex"`
	Plate     string  `json:"plate"`
	Color     float64 `json:"color"`
	ColorName string  `json:"colorName"`
	Insurance bool    `json:"insurance"`
	Forsakrad bool    `json:"forsakrad"`
}

func fetchVehicles() error {
	charactersHex := getCharactersHexes()

	stmt := `
		SELECT
			plate,
			vehicle,
			owner
		FROM owned_vehicles WHERE owner IN (?` + strings.Repeat(",?", len(charactersHex)-1) + `)`
	rows, err := db.Query(stmt, charactersHex...)

	if err != nil {
		return err
	}

	for rows.Next() {
		var plate, owner, dbVehicleJson string

		err = rows.Scan(&plate, &dbVehicleJson, &owner)

		if err != nil {
			return err
		}

		dbVehicle := make(map[string]interface{})
		err := json.Unmarshal([]byte(dbVehicleJson), &dbVehicle)

		if err != nil {
			return err
		}

		character, err := getCharacterByHex(owner)

		if err != nil {
			return err
		}

		character.addVehicle(Vehicle{
			getJsonValue(dbVehicle, "label", "").(string),
			getJsonValue(dbVehicle, "model", "").(float64),
			plate,
			getJsonValue(dbVehicle, "color1", "").(float64),
			getJsonValue(dbVehicle, "colorName", "").(string),
			false,
			false,
		})
	}

	return nil
}
