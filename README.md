# D9ice Synchronization

Simple Go application for setting up an API for synchronization to main server.

Reads from local database and outputs JSON response through an HTTP-Server.

## Generate a .exe file with icon
https://github.com/tc-hib/go-winres
# go-winres

A simple command line tool for embedding usual resources in Windows executables built with Go:

- A manifest
- An application icon
- Version information (the Details tab in file properties)
- Other icons and cursors

You might want to directly use winres as a library too: [github.com/tc-hib/winres](https://github.com/tc-hib/winres)

## Installation

To install the go-winres command, run:

```shell
go install github.com/tc-hib/go-winres@latest
```

## Usage

Please type `go-winres help` to get a list of commands and options.

Typical usage would be:

* Run `go-winres init` to create a `winres` directory
* Modify the contents of `winres.json`
* Before `go build`, run `go-winres make`

`go-winres make` creates files named `rsrc_windows_*.syso` that `go build` automatically embeds in the executable.

The suffix `_windows_amd64` is very important.
Thanks to it, `go build` knows it should not include that object in a Linux or 386 build.