package main

import (
	"database/sql"
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"os"
	"strings"
	"time"

	"github.com/getlantern/systray"
	"github.com/gonutz/ide/w32"

	"github.com/joho/godotenv"

	_ "github.com/go-sql-driver/mysql"
)

var characters []Character
var shown bool

type Response struct {
	Code    int         `json:"code"`
	Message string      `json:"message"`
	Data    interface{} `json:"data"`
}

func Log(msg string) {
	l := log.New(os.Stdout, "", 0)
	l.SetPrefix(time.Now().Format("2006-01-02 15:04:05") + " [] ")
	l.Print(msg)
}

func handleRequests() {
	http.HandleFunc("/characters", getCharacters)
	http.HandleFunc("/sms/", getSMS)
	log.Fatal(http.ListenAndServe(":"+os.Getenv("HTTP_PORT"), nil))
}

func getCharacters(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")

	requestToken := r.Header.Get("Authorization")
	apiToken := os.Getenv("API_TOKEN")
	if len(requestToken) == 0 || requestToken != apiToken {
		data := make([]string, 0)
		j, _ := json.Marshal(Response{
			401,
			"Unauthorized",
			data,
		})
		w.WriteHeader(http.StatusUnauthorized)
		w.Write(j)
		Log("Unauthorized connection from " + r.RemoteAddr)
		return
	}

	err = fetchCharacters()
	if err != nil {
		panic(err.Error())
	}

	err = fetchVehicles()
	if err != nil {
		panic(err.Error())
	}

	err = fetchLicenses()
	if err != nil {
		panic(err.Error())
	}

	j, _ := json.Marshal(Response{
		200,
		"Lyckades hämta data",
		characters,
	})
	w.Write(j)
	Log("Data fetched from server : " + r.RemoteAddr)
}

func getSMS(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")

	requestToken := r.Header.Get("Authorization")
	apiToken := os.Getenv("API_TOKEN")
	if len(requestToken) == 0 || requestToken != apiToken {
		data := make([]string, 0)
		j, _ := json.Marshal(Response{
			401,
			"Unauthorized",
			data,
		})
		w.WriteHeader(http.StatusUnauthorized)
		w.Write(j)
		Log("Unauthorized connection from " + r.RemoteAddr)
		return
	}

	phone := strings.Replace(r.URL.Path, "/sms/", "", 1)

	sms, err := fetchSMS(phone)
	if err != nil {
		panic(err.Error())
	}

	j, _ := json.Marshal(Response{
		200,
		"Lyckades hämta data",
		sms,
	})
	w.Write(j)
	Log("Data fetched from server : " + r.RemoteAddr)
}

func getJsonValue(list map[string]interface{}, key string, def interface{}) interface{} {
	if v, ok := list[key]; ok {
		return v
	}
	return def
}

var db *sql.DB
var err error

func main() {
	shown = true
	err := godotenv.Load()
	if err != nil {
		log.Fatal("Error loading .env file")
	}

	db_host := os.Getenv("DB_HOST")
	db_port := os.Getenv("DB_PORT")
	db_user := os.Getenv("DB_USER")
	db_password := os.Getenv("DB_PASSWORD")
	db_name := os.Getenv("DB_NAME")

	db_dns := fmt.Sprintf("%s:%s@tcp(%s:%s)/%s", db_user, db_password, db_host, db_port, db_name)
	db, err = sql.Open("mysql", db_dns)
	if err != nil {
		panic(err.Error())
	}

	onExit := func() {

	}

	systray.Run(onReady, onExit)

	defer db.Close()
}

func onReady() {
	go func() {
		systray.SetTemplateIcon(IconData, IconData)
		systray.SetTitle("D9Ice Polisdator Sync")
		systray.SetTooltip("D9Ice Polisdator Sync")

		mShowConsole := systray.AddMenuItem("Show", "Show/Hide console")
		mQuitOrig := systray.AddMenuItem("Quit", "Quit the whole app")
		showConsole := func() {
			toggleWindow()
		}

		for {
			select {
			case <-mQuitOrig.ClickedCh:
				fmt.Println("Requesting quit")
				systray.Quit()
				fmt.Println("Finished quitting")
				return
			case <-mShowConsole.ClickedCh:
				showConsole()
			}
		}
	}()

	toggleWindow()
	handleRequests()
}

func toggleWindow() {
	console := w32.GetConsoleWindow()
	if console == 0 {
		return // no console attached
	}
	_, consoleProcID := w32.GetWindowThreadProcessId(console)
	if shown {
		if w32.GetCurrentProcessId() == consoleProcID {
			w32.ShowWindowAsync(console, w32.SW_HIDE)
		}
		shown = false
	} else {
		if w32.GetCurrentProcessId() == consoleProcID {
			w32.ShowWindowAsync(console, w32.SW_SHOW)
		}
		shown = true
	}
}
