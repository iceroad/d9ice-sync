package main

type SMS struct {
	Transmitter string `json:"transmitter"`
	Receiver    string `json:"receiver"`
	Message     string `json:"message"`
	Time        string `json:"time"`
	Owner       int    `json:"owner"`
}

func fetchSMS(phone string) ([]SMS, error) {
	sms := []SMS{}

	stmt := `
		SELECT
			transmitter,
			receiver,
			message,
			time,
			owner
		FROM phone_messages WHERE receiver = ?`
	rows, err := db.Query(stmt, phone)

	if err != nil {
		return sms, err
	}

	for rows.Next() {
		var newSms SMS
		err = rows.Scan(
			&newSms.Transmitter,
			&newSms.Receiver,
			&newSms.Message,
			&newSms.Time,
			&newSms.Owner)

		if err != nil {
			return sms, err
		}

		sms = append(sms, newSms)
	}

	return sms, nil
}
