module D9iceSync

go 1.16

require (
	github.com/getlantern/systray v1.1.0
	github.com/go-sql-driver/mysql v1.6.0
	github.com/gonutz/ide v0.0.0-20200517034207-df64a3832118
	github.com/joho/godotenv v1.3.0
	github.com/stretchr/testify v1.6.1 // indirect
	golang.org/x/sys v0.0.0-20210615035016-665e8c7367d1 // indirect
)
