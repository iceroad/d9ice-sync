package main

import "errors"

type Character struct {
	Identifier  string    `json:"identifier"`
	Firstname   string    `json:"firstname"`
	Lastname    string    `json:"lastname"`
	Birth       string    `json:"birth"`
	Height      *string   `json:"height"`
	Sex         *string   `json:"sex"`
	Lastdigits  string    `json:"lastdigits"`
	Job         *string   `json:"job"`
	Job_name    *string   `json:"job_name"`
	Job_station *string   `json:"job_station"`
	Phone       *string   `json:"phone"`
	Vehicles    []Vehicle `json:"vehicles"`
	Licenses    []string  `json:"licenses"`
}

func (c *Character) addVehicle(v Vehicle) Character {
	c.Vehicles = append(c.Vehicles, v)
	return *c
}

func (c *Character) addLicense(l string) Character {
	c.Licenses = append(c.Licenses, l)
	return *c
}

func fetchCharacters() error {
	characters = []Character{}

	rows, err := db.Query(`SELECT 
		users.identifier,
		IFNULL(users.firstname, ''),
		IFNULL(users.lastname, ''),
		DATE_FORMAT(users.dateofbirth,'%Y%m%d') AS birth,
		IFNULL(users.height, ''),
		IFNULL(users.sex, ''),
		users.lastdigits,
		IFNULL(users.job, ''),
		IFNULL(job_grades.label, '') AS job_label,
		IFNULL(jobs.label, '') AS job_station,
		IFNULL(users.phone_number, '') AS phone
	FROM users 
	LEFT JOIN job_grades ON users.job = job_grades.job_name AND users.job_grade = job_grades.grade 
	LEFT JOIN jobs ON users.job = jobs.name
	WHERE 
		users.dateofbirth <> '' 
	AND users.dateofbirth IS NOT NULL 
	AND users.lastdigits <> '' 
	AND users.lastdigits IS NOT NULL
	AND users.identifier <> ''
	`)

	if err != nil {
		return err
	}

	for rows.Next() {
		var character Character
		err = rows.Scan(
			&character.Identifier,
			&character.Firstname,
			&character.Lastname,
			&character.Birth,
			&character.Height,
			&character.Sex,
			&character.Lastdigits,
			&character.Job,
			&character.Job_name,
			&character.Job_station,
			&character.Phone)

		if err != nil {
			return err
		}

		characters = append(characters, character)
	}

	return nil
}

func getCharactersHexes() []interface{} {
	charactersHex := make([]interface{}, len(characters))
	for i, character := range characters {
		charactersHex[i] = character.Identifier
	}

	return charactersHex
}

func getCharacterByHex(hex string) (*Character, error) {
	for i, v := range characters {
		if v.Identifier == hex {
			return &characters[i], nil
		}
	}

	return &Character{}, errors.New("No character found")
}
