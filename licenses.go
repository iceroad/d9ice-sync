package main

import "strings"

func fetchLicenses() error {
	charactersHex := getCharactersHexes()

	stmt := `
		SELECT
			owner, type
		FROM user_licenses WHERE owner IN (?` + strings.Repeat(",?", len(charactersHex)-1) + `)`
	rows, err := db.Query(stmt, charactersHex...)

	if err != nil {
		return err
	}

	for rows.Next() {
		var owner, licenseName string

		err = rows.Scan(&owner, &licenseName)

		if err != nil {
			return err
		}

		character, err := getCharacterByHex(owner)

		if err != nil {
			return err
		}

		character.addLicense(licenseName)
	}

	return nil
}
